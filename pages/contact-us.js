import Head from 'next/head'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import ContactForm from './ContactForm'

export default function ContactUs() {
  return (
    <>
      <Head>
        <title>Contact Us Barns &amp; Silos</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar/>
      <section className="hero_bg grid justify-items-center">
        <div className=" section_container flex flex-wrap justify-between">
          <div className="hero_content contact md:mt-12 mr-4 flex-grow">
            <h5>Get in Touch</h5>
            <p className="pt-4">We’re a team of professionals who are excited about unique ideas and help your company to create amazing identity by crafting top-notch UI/UX.</p>
           
          </div>
          <div className="contact_form w-full py-4 mt-6 md:0">
            <ContactForm/>
          </div>
        </div>
      </section>
      <Footer/>
    </>
  )
}
