import '../styles/globals.css'
import '../styles/tailwind_animation.css'
import "../styles/sass/barns_silos.scss";
import Head from 'next/head';
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import * as gtag from '../lib/gtag'

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url)
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])
  return (
    <>
      <Head>
        <link rel="preload"
  href="/fonts/stylesheet.css"
  />
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" 
  // rel="stylesheet"
    rel="preload"
    as="font"
    type="font/woff2"/>
      </Head>
        <Component {...pageProps} />
    </>
  )
}

export default MyApp
