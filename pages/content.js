
import Head from 'next/head'
import Link from 'next/link'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import FAQ from '../components/FAQ'

export default function Content({faqs}) {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
                                    {/* <!-- Primary Meta Tags --> */}
        <title>Content Barns &amp; Silos</title>
        <meta name="title" content="Content Barns &amp; Silos"/>
        <meta name="description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://www.barnsandsilos.com/content"/>
        <meta property="og:title" content="Content Barns &amp; Silos"/>
        <meta property="og:description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>
        <meta property="og:image" content="/images/meta.jpg"/>

        {/* <!-- Twitter --> */}
        <meta property="twitter:card" content="summary_large_image"/>
        <meta property="twitter:url" content="https://www.barnsandsilos.com/content"/>
        <meta property="twitter:title" content="Content Barns &amp; Silos"/>
        <meta property="twitter:description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>
        <meta property="twitter:image" content="/images/meta.jpg"/>
      </Head>
      <Navbar/>
      <section className="hero_bg grid justify-items-center">
        <div className=" section_container items-center flex flex-wrap justify-between">
          <div className="hero_content mr-4 flex-grow">
            <h5>Content</h5>
            <p className="pt-4">Content helps your company to have the unique plan for your brand. We can champion your business with creative contents.</p>
            <div className="pt-4 md:pt-8 grid hero_grid  justify-between">
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Copywriting</p>
                </div>
                <div className="flex py-2 pr-3 items-center">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Illustration</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Motion Design</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Social Media</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Interactive Media</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Photography &amp; Video</p>
                </div>
            </div>
          </div>
          <div className="hero_img flex-grow py-6 flex">
            <img src="/images/hero2.svg" alt="Woman viewing content"/>
          </div>
        </div>
      </section>
      <section className="benefits_bg py-10">
        <div className="section_container">
          <div className="top_flex flex py-8 flex-wrap items-center justify-between">
            <h5 className="flex-grow mr-4">Our Process</h5>
            <p className="flex-grow">Having worked on so many projects, we have been able to come
up with the implementation process which leaves both, Us and
our clients’ happy from start to end of the project.</p>
          </div>
          <div className="flex_body grid justify-between">
            <div className="flex_item py-4 pr-4">
              <div className="">
                <img src="/images/numbers/no1.svg" alt="Number one"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Discover</h4>
                <p>We start by understanding
your brand, business needs, goals
and objectives. These helps us 
determine an appropriate plan
and timeline for your project.</p>
              </div>
            </div>
            
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no2.svg" alt="Number two"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Develop</h4>
                <p>After the discovery phase, we’ll
work on messaging, core values
and personality to define who
your brand is and what it
stands for.</p>
              </div>
            </div>
            <div className="flex_item py-4  pr-4">
              <div className="">
              <img src="/images/numbers/no3.svg" alt="Number three"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Design</h4>
                <p>From logos to business swag,
we’ll design everything you need
to convey your new-found
brand identity.</p>
              </div>
            </div>
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no4.svg" alt="Number four"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Implement</h4>
                <p>We’ll help you implement your
newly formed brand strategy
and visual identity across all
your digital presences.</p>
              </div>
            </div>
           
          </div>
        </div>
      </section>
      {/* faq section  */}
        <section className="faq py-10">
            <div className="section_container ">
                <FAQ faqs={faqs}/>
            </div>
        </section>
      <Footer/>
    </>
  )
}
const space = process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID
const accessToken = process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN;
const client = require('contentful').createClient({
    space: space,
    accessToken: accessToken,
})
export async function getStaticProps(){
    let data = await client.getEntries({
      content_type: 'barnsAndSilosFaq',
      order: 'fields.questionNumber'
    })
    return {
        props:{
            faqs:data.items
        }
    }
}