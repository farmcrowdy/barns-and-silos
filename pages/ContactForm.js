import axios from 'axios';
import React, {useState} from 'react'
import Spinner from '../components/Spinner';
import {useRouter} from 'next/router';

export default function ContactForm() {
    let Router = useRouter();
    // form states 
    const [nameErr, setNameErr] = useState("");
    const [phoneErr, setPhoneErr] = useState("");
    const [emailErr, setEmailErr] = useState("");
    const [descriptionErr, setDescriptionErr] = useState("");
    const [ serviceErr, setServiceErr] = useState("");
    const [formState, setFormState]= useState({});
    const [formErr, setFormErr]= useState("");
    const [spinner, setSpinner]= useState(false);
    // forward arrow
    const arrowRight =  <svg className="inline-block ml-2 relative" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clipPath="url(#clip0)">
                            <path d="M31.6334 15.1151C31.633 15.1147 31.6327 15.1143 31.6322 15.1139L25.1007 8.61393C24.6114 8.127 23.82 8.12881 23.3329 8.61818C22.8459 9.1075 22.8478 9.89893 23.3371 10.3859L27.7224 14.7499H1.25C0.559625 14.7499 0 15.3096 0 15.9999C0 16.6903 0.559625 17.2499 1.25 17.2499H27.7223L23.3372 21.6139C22.8479 22.1009 22.846 22.8924 23.333 23.3817C23.8201 23.8711 24.6116 23.8728 25.1008 23.3859L31.6323 16.8859C31.6327 16.8856 31.633 16.8851 31.6334 16.8847C32.123 16.3961 32.1214 15.6021 31.6334 15.1151Z" fill="#082933"/>
                            </g>
                            <defs>
                            <clipPath id="clip0">
                            <rect width="32" height="32" fill="white"/>
                            </clipPath>
                            </defs>
                        </svg>;
      // handle onChange 
      const onChange = (e) =>{
        const name = e.target.name;
        const value = e.target.value;
        setFormState({...formState,[name]:value});
        console.log(value)
    }
    // check if email is correct 
    const emailChecker = (e)=>{
        if(!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(e.target.value))){
            setEmailErr("Invalid email");
        }else{
            setEmailErr(""); 
        }
    }
    // check if any of the field is empty or has error 
    // const formReady = !formState.Name?.length || !formState.Phone?.length || !formState.Email?.length || !formState.Description?.length || !formState.Service?.length || emailErr;
    const formReady = !formState.Name?.length || !formState.Phone?.length || !formState.Email?.length || emailErr;
    // submit form 
    const formUrl = process.env.NEXT_PUBLIC_FORMSUBMIT_API_KEY;
    const handleSubmit = async (e)=> {
        e.preventDefault();
        setSpinner(true);
        try{
            setFormErr("");
            let res = await axios.post(formUrl, formState, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            })
            console.log(formState)
            if(res.data.success){
                // set spinner to false 
                setSpinner(false);
                setFormErr("");
                Router.push('/contact-us-success');
            }else{
                 // set spinner to false 
                 setSpinner(false);
                // catch error 
                setFormErr(res.data.message);
            }

        }catch(e){
             // set spinner to false 
             setSpinner(false);
            setFormErr(e.message);
        }
    }
    return (
        <form onSubmit={handleSubmit}>
            {formErr ?
            <div className="flex items-center mb-2 justify-center"> 
                <svg className="inline-block mr-3" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                    <path d="M19.6809 15.4445L12.0364 2.20381C11.6113 1.46756 10.85 1.02795 9.99975 1.02795C9.14955 1.02795 8.38815 1.46756 7.96307 2.20381L0.318593 15.4444C-0.106523 16.1808 -0.106523 17.0599 0.318593 17.7962C0.74371 18.5325 1.50504 18.9721 2.35527 18.9721H17.6442C18.4944 18.9721 19.2558 18.5325 19.6809 17.7962C20.1061 17.0599 20.1061 16.1808 19.6809 15.4445ZM18.6662 17.2104C18.4529 17.5798 18.0708 17.8004 17.6442 17.8004H2.35527C1.92863 17.8004 1.5466 17.5798 1.33332 17.2104C1.12004 16.8409 1.12004 16.3998 1.33332 16.0304L8.97787 2.78971C9.19115 2.42026 9.57318 2.19971 9.99979 2.19971C10.4263 2.19971 10.8084 2.42026 11.0217 2.78971L18.6662 16.0304C18.8795 16.3998 18.8795 16.8409 18.6662 17.2104Z" fill="#BE0000"/>
                    <path d="M10.5858 6.87537H9.41406V12.734H10.5858V6.87537Z" fill="black"/>
                    <path d="M10.0001 13.9058C9.5693 13.9058 9.21887 14.2563 9.21887 14.687C9.21887 15.1177 9.5693 15.4682 10.0001 15.4682C10.4308 15.4682 10.7812 15.1177 10.7812 14.687C10.7812 14.2563 10.4308 13.9058 10.0001 13.9058Z" fill="black"/>
                    </g>
                    <defs>
                    <clipPath id="clip0">
                    <rect width="20" height="20" fill="white"/>
                    </clipPath>
                    </defs>
                </svg>
                <span className=" text-red-800 text-sm">{formErr}</span> 
            </div>
            : ""}
           <div className="form_inputs grid">
                <div className="pb-3 md:pb-6">
                    <label htmlFor="name">Your Name</label>
                    <input name="Name" id="name" type="text" placeholder="Enter your name here" onChange={onChange} onBlur={e=>{e.target.value == "" ? setNameErr("Please enter your name"): setNameErr("")}}/>
                    {nameErr ? <span className="block text-red-800 text-sm">{nameErr}</span>: ""}

                </div>
                <div className="pb-3 md:pb-6">
                    <label htmlFor="phone">Phone Number</label>
                    <input type="tel" name="Phone" id="phone" placeholder="Enter your phone number here" onChange={onChange} onBlur={e=>{e.target.value == "" ? setPhoneErr("Please enter your phone number"): setPhoneErr("")}}/>
                    {phoneErr ? <span className="block text-red-800 text-sm">{phoneErr}</span>: ""}

                </div>
            </div>
           <div className="form_inputs grid">
                <div>
                    <div className="pb-3 md:pb-6">
                        <label htmlFor="email">Email</label>
                        <input type="email" name="Email" id="email" placeholder="Enter your email here" onChange={onChange} onBlur={e=>emailChecker(e)}/>
                        {emailErr ? <span className="block text-red-800 text-sm">{emailErr}</span>: ""}

                    </div>
                    <div className="pb-3 md:pb-6">
                        <label htmlFor="budget">What services do you want?</label>
                        <select  onChange={onChange} name="Service" onBlur={e=>{e.target.value == "" ? setServiceErr("Please select a service"): setServiceErr("")}}>
                            <option value="">Select One</option>
                            <option value="App Development">App Development</option>
                            <option value="Branding &amp; Positioning">Branding &amp; Positioning</option>
                            <option value="Business Consulting">Business Consulting</option>
                            <option value="Copywriting">Copywriting</option>
                            <option value="Enterprise CMS">Enterprise CMS</option>
                            <option value="Photography &amp; Video">Photography &amp; Video</option>
                            <option value="Research &amp; Data">Research &amp; Data</option>
                            <option value="Social Media">Social Media</option>
                            <option value="User Research &amp; Testing">User Research &amp; Testing</option>
                            <option value="UX Design">UX Design</option>
                            <option value="Visual Design">Visual Design</option>
                            <option value="Web Development">Web Development</option>                        
                        </select>
                        {/* {serviceErr ? <span className="block text-red-800 text-sm">{serviceErr}</span>: ""} */}
                    </div>
                </div>
                <div className="pb-3 md:pb-6">
                    <label htmlFor="description">Short Description</label>
                    <textarea name="Description" id="description" placeholder="Describe the project here" onChange={onChange}  onBlur={e=>{e.target.value == "" ? setDescriptionErr("Please enter your message"): setDescriptionErr("")}}></textarea>
                    {/* {descriptionErr ? <span className="block text-red-800 text-sm">{descriptionErr}</span>: ""} */}

                </div>
               
            </div>
            <div className="form_inputs grid">
                <div className="pb-3 md:pb-6">
                    <label htmlFor="budget">Estimated Budget</label>
                    <input name="Budget" id="budget" type="number" placeholder="Enter your budget here" onChange={onChange} />
                </div>
                <div className="pb-3 md:pb-6">
                    <label htmlFor="currency">Currency</label>
                    <select id="currency" name="currency">
                        <option value="">Select currency</option>
                        <option value="Naira">Naira</option>
                        <option value="Dollar">Dollar</option>
                    </select>
                    {/* {phoneErr ? <span className="block text-red-800 text-sm">{phoneErr}</span>: ""} */}
                    {/* <input type="tel" name="Phone" id="phone" placeholder="Enter your phone number here" onChange={onChange} onBlur={e=>{e.target.value == "" ? setPhoneErr("Please enter your phone number"): setPhoneErr("")}}/> */}
                </div>
            </div>
          
            <div className="py-6 md:text-center">
                
                <button disabled={formReady} className={`w-full md:w-auto md:px-12 py-3 ${formReady ? "opacity-50 disable cursor-not-allowed":"" }`}>Get in Touch
                    {!spinner ? arrowRight:<Spinner/>}
                </button>
            </div>
        </form>
    )
}


