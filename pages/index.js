import Head from 'next/head'
import Link from 'next/link'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'

export default function Home() {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
              {/* <!-- Primary Meta Tags --> */}
        <title>Barns &amp; Silos - A Media Digital Agency</title>
        <meta name="title" content="Barns &amp; Silos - A Media Digital Agency"/>
        <meta name="description" content=" Barns &amp; Silos is a Media Digital Agency that harnesses the use of cutting-edge technologies and best-in-class talent to offer services to grow your customer base and revenue."/>
        {/* facebook pixel  */}
        <meta name="facebook-domain-verification" content="dyuejsmuoy6ou5i8f2kgo7jpt87051" />
        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://www.barnsandsilos.com/"/>
        <meta property="og:title" content="Barns &amp; Silos - A Media Digital Agency"/>
        <meta property="og:description" content=" Barns &amp; Silos is a Media Digital Agency that harnesses the use of cutting-edge technologies and best-in-class talent to offer services to grow your customer base and revenue."/>
        <meta property="og:image" content="/images/meta.jpg"/>

        {/* <!-- Twitter --> */}
        <meta property="twitter:card" content="summary_large_image"/>
        <meta property="twitter:url" content="https://www.barnsandsilos.com/"/>
        <meta property="twitter:title" content="Barns &amp; Silos - A Media Digital Agency"/>
        <meta property="twitter:description" content=" Barns &amp; Silos is a Media Digital Agency that harnesses the use of cutting-edge technologies and best-in-class talent to offer services to grow your customer base and revenue."/>
        <meta property="twitter:image" content="/images/meta.jpg"/>
      </Head>
      <Navbar/>
      <section className="hero_bg grid justify-items-center">
        <div className=" section_container items-center flex flex-wrap justify-between">
          <div className="hero_content mr-4 flex-grow">
            <h5>360&#730;  Integrated Marketing Solutions for <span>Agripeneurs</span> and <span>SMEs</span></h5>
            <div className="pt-8 md:pt-20">
              <Link href="/contact-us">
                <a className="md:py-4 sm:py-3 py-2 sm:px-12 px-8 inline-block">Get in Touch
                  <svg className="relative inline-block ml-2" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                    <path d="M31.6334 15.1151C31.633 15.1148 31.6327 15.1143 31.6322 15.114L25.1007 8.61397C24.6114 8.12703 23.82 8.12884 23.3329 8.61821C22.8459 9.10753 22.8478 9.89896 23.3371 10.386L27.7224 14.75H1.25C0.559625 14.75 0 15.3096 0 16C0 16.6903 0.559625 17.25 1.25 17.25H27.7223L23.3372 21.614C22.8479 22.101 22.846 22.8924 23.333 23.3817C23.8201 23.8711 24.6116 23.8728 25.1008 23.386L31.6323 16.886C31.6327 16.8856 31.633 16.8851 31.6334 16.8848C32.123 16.3961 32.1214 15.6021 31.6334 15.1151Z" fill="#082933"/>
                    </g>
                    <defs>
                    <clipPath id="clip0">
                    <rect width="32" height="32" fill="white"/>
                    </clipPath>
                    </defs>
                  </svg>
                </a>
              </Link>
            </div>
          </div>
          <div className="hero_img flex-grow py-6 flex">
            <img src="/images/home.svg" alt="Patterned background"/>
          </div>
        </div>
      </section>
      <section className="benefits_bg py-10">
        <div className="section_container">
          <div className="top_flex flex py-8 flex-wrap items-center justify-between">
            <h5 className="flex-grow mr-4">Benefits</h5>
            <p className="flex-grow">Projects are handled by the team responsible for building Nigeria’s
biggest digital agricultural platform, Farmcrowdy. So, our clients
rest easy knowing the benefits their projects gets;</p>
          </div>
          <div className="flex_body flex flex-wrap justify-between">
            <div className="flex_item py-4">
              <div className="">
                <img src="/images/benefits/img1.svg" alt="Quality icon"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Quality</h4>
                <p>Leaving a client totally satisfied
at the end of project has led us to
incorporate an impressive quality
in all our processes.</p>
              </div>
            </div>
            
            <div className="flex_item py-4">
              <div className="">
                <img src="/images/benefits/img2.svg" alt="Reliability icon"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Reliability</h4>
                <p>We bring predictability with our
consistent performance, keeping
our promises and also timelines.</p>
              </div>
            </div>
            <div className="flex_item py-4">
              <div className="">
                <img src="/images/benefits/img3.svg" alt="scale icon"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Scale</h4>
                <p>At Barns &amp; Silos - A Media Digital Agency, you get a detailed fact-based strategy leading to growth of your customer base and revenue.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="capability py-10 relative overflow-hidden" id="services">
        <div className="section_container ">
          <div className="top_flex flex py-8 flex-wrap items-center justify-between">
            <h5 className="flex-grow mr-4">Capabilities</h5>
            <p className="flex-grow">We are passionate on creating deeply connected brands, services and campaigns to enable stable and successful relationships between businesses and customers.</p>
          </div>
          <div className="top_flex flex flex-wrap py-8 md:justify-end">
            <div className="flex-grow capability_item">
              <div className="inner_flex flex py-6 flex-wrap justify-between">
                <h4 className="py-2 mr-2">Insights &amp; Strategy</h4>
                <div className="inner_content flex-grow">
                  <div className=" grid inner_content_grid  justify-between">
                    <div className="flex py-2 items-center pr-3">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">SEO/SEM</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Innovation</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Research &amp; Data</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Go to Market</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Business Consulting</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Branding &amp; Positioning</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="border"></div>
              <div className="inner_flex flex py-6 flex-wrap justify-between">
                <h4 className="py-2 mr-2">Content</h4>
                <div className="inner_content flex-grow">
                  <div className=" grid inner_content_grid  justify-between">
                    <div className="flex py-2 items-center pr-3">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Copywriting</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Illustration</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Motion Design</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Social Media</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Interactive Media</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Photography &amp; Video</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="border"></div>
              <div className="inner_flex flex py-6 flex-wrap justify-between">
                <h4 className="py-2 mr-2">Tech &amp; Scale</h4>
                <div className="inner_content flex-grow">
                  <div className=" grid inner_content_grid  justify-between">
                    <div className="flex py-2 items-center pr-3">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Emerging Tech</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Enterprise CMS</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Web Development</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Application Development</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="border"></div>
              <div className="inner_flex flex py-6 flex-wrap justify-between">
                <h4 className="py-2 mr-2">Experience Design</h4>
                <div className="inner_content flex-grow">
                  <div className=" grid inner_content_grid  justify-between">
                    <div className="flex py-2 items-center pr-3">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">UX Design</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Visual Design</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Editorial Design</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">Information Architecture</p>
                    </div>
                    <div className="flex py-2 pr-3 items-center">
                        <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                        </svg>
                        <p className="flex-grow ml-2">User Research &amp; Testing</p>
                    </div>
  
                  </div>
                </div>
              </div>
            </div>
            <div className="absolute bg_image">
              <img className="" src="/images/bg/capability.svg" alt="People joining hands"/>
            </div>
          </div>
        </div>
        
      </section>
      <Footer/>
    </>
  )
}
