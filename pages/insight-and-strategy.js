import Head from 'next/head'
import Link from 'next/link'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import FAQ from '../components/FAQ'

export default function ExperienceDesign({faqs}) {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
                         {/* <!-- Primary Meta Tags --> */}
        <title>Insight &amp; Strategy Barns &amp; Silos</title>
        <meta name="title" content="Insight &amp; Strategy Barns &amp; Silos"/>
        <meta name="description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://www.barnsandsilos.com/insight-and-strategy"/>
        <meta property="og:title" content="Insight &amp; Strategy Barns &amp; Silos"/>
        <meta property="og:description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>
        <meta property="og:image" content="/images/meta.jpg"/>

        {/* <!-- Twitter --> */}
        <meta property="twitter:card" content="summary_large_image"/>
        <meta property="twitter:url" content="https://www.barnsandsilos.com/insight-and-strategy"/>
        <meta property="twitter:title" content="Insight &amp; Strategy Barns &amp; Silos"/>
        <meta property="twitter:description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>
        <meta property="twitter:image" content="/images/meta.jpg"/>
      </Head>
      <Navbar/>
      <section className="hero_bg grid justify-items-center">
        <div className=" section_container items-center flex flex-wrap justify-between">
          <div className="hero_content mr-4 flex-grow">
            <h5>Insight &amp; Strategy</h5>
            <p className="pt-4">Find opportunities for market expansion, growth of market share among your target segments of growers and increase the share of wallet you have among your current customers</p>
            <div className="pt-4 md:pt-8 grid hero_grid  justify-between">
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">SEO/SEM</p>
                </div>
                <div className="flex py-2 pr-3 items-center">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Innovation</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Research &amp; Data</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Go to Market</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Business Consulting</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Branding &amp; Positioning</p>
                </div>
            </div>
          </div>
          <div className="hero_img flex-grow py-6 flex">
            <img src="/images/hero4.svg" alt="Stratagists"/>
          </div>
        </div>
      </section>
      <section className="benefits_bg py-10">
        <div className="section_container">
          <div className="top_flex flex py-8 flex-wrap items-center justify-between">
            <h5 className="flex-grow mr-4">Our Process</h5>
            <p className="flex-grow">Having worked on so many projects, we have been able to come
up with the implementation process which leaves both, Us and
our clients’ happy from start to end of the project.</p>
          </div>
          <div className="flex_body grid justify-between">
            <div className="flex_item py-4 pr-4">
              <div className="">
                <img src="/images/numbers/no1.svg" alt="Number one"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Market Research</h4>
                <p>We start by understanding
your brand, business needs, goals
and objectives. These helps us 
make an informed decisions as
we progress.</p>
              </div>
            </div>
            
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no2.svg" alt="Number two"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Collaborate</h4>
                <p>We work with brands in a
hands-on way to outline their
objectives as well as their short
and long-term goals. Then, we
bring them to life.</p>
              </div>
            </div>
            <div className="flex_item py-4  pr-4">
              <div className="">
              <img src="/images/numbers/no3.svg" alt="Number three"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Strategy</h4>
                <p>Combining research, data, and
a human-first approach, to
create marketing strategies that
keep your goals, your audience,
and your objectives in mind.</p>
              </div>
            </div>
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no4.svg" alt="Number four"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Execute</h4>
                <p>From messaging to optimizing
creative assets and everything in
between, we execute your strategy
and connect your brand with those
who matter most—your customers.</p>
              </div>
            </div>
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no5.svg" alt="Number five"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Optimize</h4>
                <p>We take an agile approach by
adjusting our digital marketing 
strategy, optimizing campaigns
and implementing new findings
to improve your ROI.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* faq section  */}
        <section className="faq py-10">
            <div className="section_container ">
                <FAQ faqs={faqs}/>
            </div>
        </section>
      <Footer/>
    </>
  )
}
const space = process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID
const accessToken = process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN;
const client = require('contentful').createClient({
    space: space,
    accessToken: accessToken,
})
export async function getStaticProps(){
    let data = await client.getEntries({ 
      content_type: 'barnsAndSilosFaq',
      order: 'fields.questionNumber'
    })
    return {
        props:{
            faqs:data.items
        }
    }
}
