import Head from 'next/head'
import Link from 'next/link'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import FAQ from '../components/FAQ'

export default function ExperienceDesign({faqs}) {
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
                                 {/* <!-- Primary Meta Tags --> */}
        <title>Experience Design Barns &amp; Silos</title>
        <meta name="title" content="Experience Design Barns &amp; Silos"/>
        <meta name="description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://www.barnsandsilos.com/experience-design"/>
        <meta property="og:title" content="Experience Design Barns &amp; Silos"/>
        <meta property="og:description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>
        <meta property="og:image" content="/images/meta.jpg"/>

        {/* <!-- Twitter --> */}
        <meta property="twitter:card" content="summary_large_image"/>
        <meta property="twitter:url" content="https://www.barnsandsilos.com/experience-design"/>
        <meta property="twitter:title" content="Experience Design Barns &amp; Silos"/>
        <meta property="twitter:description" content=" Barns and Silos provides marketing solutions such as content creation, branding, visual design e.t.c for Agripreneurs and SMEs."/>
        <meta property="twitter:image" content="/images/meta.jpg"/>
      </Head>
      <Navbar/>
      <section className="hero_bg grid justify-items-center">
        <div className=" section_container items-center flex flex-wrap justify-between">
          <div className="hero_content mr-4 flex-grow">
            <h5>Experience Design</h5>
            <p className="pt-4">UI/UX design gives the appearance of your brand’s behavior. We at <span className="font-bold">Barns and Silos</span> have the power to discover unique insights that can drive powerful creativity that works for you. We deliver value exactly where it’s needed, at the business end.</p>
            <div className="pt-4 md:pt-8 grid hero_grid  justify-between">
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">UX Design</p>
                </div>
                <div className="flex py-2 pr-3 items-center">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Visual Design</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">User Research &amp; Testing</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Information Architecture</p>
                </div>
                <div className="flex py-2 items-center pr-3">
                    <svg className="" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="6" cy="6" r="6" fill="#C89B41"/>
                    </svg>
                    <p className="flex-grow ml-2">Editorial Design</p>
                </div>
            </div>
          </div>
          <div className="hero_img flex-grow py-6 flex">
            <img src="/images/hero1.svg" alt="Woman designing"/>
          </div>
        </div>
      </section>
      <section className="benefits_bg py-10">
        <div className="section_container">
          <div className="top_flex flex py-8 flex-wrap items-center justify-between">
            <h5 className="flex-grow mr-4">Our Process</h5>
            <p className="flex-grow">Having worked on so many projects, we have been able to come
up with the implementation process which leaves both, Us and
our clients’ happy from start to end of the project.</p>
          </div>
          <div className="flex_body grid justify-between">
            <div className="flex_item py-4 pr-4">
              <div className="">
                <img src="/images/numbers/no1.svg" alt="Number one"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Identify</h4>
                <p>We start by understanding
your brand, business needs, goals
and objectives. These helps us 
determine an appropriate plan
and timeline for your project.</p>
              </div>
            </div>
            
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no2.svg" alt="Number two"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Research</h4>
                <p>We conduct a deep research
on your competors, industry, 
audience and an interview of all
internal stalkholders to buildup a
tailored strategy and design.</p>
              </div>
            </div>
            <div className="flex_item py-4  pr-4">
              <div className="">
              <img src="/images/numbers/no3.svg" alt="Number three"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>UI/UX</h4>
                <p>A sitemap and wireframe is
created. Helps to determine the
key features, functionalities and
structure of website to be
developed.</p>
              </div>
            </div>
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no4.svg" alt="Number four"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Content Strategy</h4>
                <p>We would work collaboratively
with your team to determine
the needed content and its 
placement to achieve maximum
impact.</p>
              </div>
            </div>
            <div className="flex_item py-4 pr-4">
              <div className="">
              <img src="/images/numbers/no5.svg" alt="Number five"/>
              </div>
              <div className="py-4 md:pt-6">
                <h4>Design</h4>
                <p>At this stage, the long awaited
visual elements required to make
your website functional is
designed.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* faq section  */}
        <section className="faq py-10">
            <div className="section_container ">
                <FAQ faqs={faqs}/>
            </div>
        </section>
      <Footer/>
    </>
  )
}
const space = process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID
const accessToken = process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN;
const client = require('contentful').createClient({
    space: space,
    accessToken: accessToken,
})
export async function getStaticProps(){
    let data = await client.getEntries({ 
      content_type: 'barnsAndSilosFaq',
      order: 'fields.questionNumber'
    })
    return {
        props:{
            faqs:data.items
        }
    }
}
