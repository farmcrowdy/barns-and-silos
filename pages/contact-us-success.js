import Head from 'next/head'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'

export default function ContactUsSuccess() {
  return (
    <>
      <Head>
        <title>Contact Us Barns &amp; Silos</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar/>
      <section className="hero_bg grid justify-items-center">
        <div className=" section_container flex flex-wrap justify-between">
          <div className="hero_content contact md:mt-12 mr-4 flex-grow">
            <h5>Thank you for getting in touch.</h5>
            <p className="pt-4">We appreciate you contacting us. One of our colleagues will get back in touch with you soon!</p>
            {/* <p className="pt-10"> Our team will get back to you as soon as possible.</p> */}
          </div>
          <div className="contact_form py-4 mt-6 md:0">
          <img src="/images/contact.svg" alt="Woman " className=""/>
          </div>
        </div>
      </section>
      <Footer/>
    </>
  )
}
