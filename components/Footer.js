import Link from 'next/link'
import React from 'react'

export default function Footer() {
    return (
        <>
            <section className="get_intouch_bg">
                <div className=" text-center section_container py-10 md:py-14 lg:py-24">
                    <p>Got a Project in Mind?</p>
                   <Link href="/contact-us">
                        <a className="font-bold md:text-4xl text-xl">Get In Touch?
                            <svg className="inline-block relative" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M29.9476 14.1206L29.9471 14.12L29.9461 14.1189C29.9466 14.1196 29.9472 14.1202 29.9476 14.1206ZM29.9476 14.1206C29.9478 14.1209 29.948 14.1211 29.9478 14.1209L29.9476 14.1206ZM21.726 10.5337L24.8283 13.621H2.12903C1.22933 13.621 0.5 14.3503 0.5 15.25C0.5 16.1497 1.22933 16.879 2.12903 16.879H24.8283L21.7261 19.9663L21.7261 19.9663C21.0884 20.601 21.0859 21.6324 21.7206 22.2701C22.3554 22.908 23.3869 22.9101 24.0244 22.2756L23.6717 21.9212L24.0244 22.2756L29.912 16.4164L29.9239 16.4047L29.9242 16.4043C29.9263 16.4023 29.9282 16.4003 29.93 16.3984C30.562 15.7627 30.56 14.7356 29.9303 14.1019C29.9301 14.1017 29.9299 14.1015 29.9297 14.1013C29.9273 14.0988 29.9246 14.0961 29.9216 14.0932L24.0244 8.22436L24.0244 8.22436C23.3867 7.58979 22.3553 7.59214 21.7206 8.22989C21.0859 8.86758 21.0883 9.89901 21.726 10.5337L21.726 10.5337ZM29.9242 16.4043C29.9241 16.4044 29.924 16.4045 29.9238 16.4046L29.9242 16.4043Z" fill="#082933" stroke="#082933"/>
                            </svg>
                        </a>
                    </Link>
                </div>
            </section>  
            <footer>
               <div className="section_container py-4">
                   <div className="seo_content">
                       <p>Barns &amp; Silos is a Media Digital Agency that harnesses the use of cutting-edge technologies, multi-channel marketing, and best-in-class talent to offer services to drive traffic, increase sales, and create brand awareness. We use powerful data integration, marketing automation, and enablement solutions to create successful digital marketing strategies.</p>

                       <p>By leveraging robust marketing insights and smart targeting, we create behavior-changing ideas and experiences that will deliver value to your brand. Supported by our proprietary creative intelligence process, user experience/user interface, unique tools, and global partners, we put data at the heart of everything we do to orchestrate experiences that deliver creativity with precision and purpose. Some of our services include; Design, Brand Development, Content Marketing, Email Marketing, SEO/SEM, Inbound Marketing, Branding, Packaging, Print Production, Website Development, Application Development, User Research &amp; Testing, UX Design, Visual Design, and many more.</p>

                       <p>With technology and innovation, we challenge ourselves to move our clients forward, while continually focused on bringing them solutions that are simple, creative, effective, and efficient to deliver value to their business. We are a team of professionals who are excited about unique ideas to handle your company’s marketing for efficient growth, awareness, and sales boost. Our approach to data and technology allows us to orchestrate comprehensive experiences in owned, earned, and paid channels to optimize the value exchange at every touchpoint hence we empower your business across the entire customer journey.</p>
                   </div>
                   <div className="flex flex-wrap justify-between mt-4">
                       <div className="links mr-4 pt-8">
                           <h4>Explore</h4>
                           <ul>
                               <li>
                                   <Link href="/">
                                        <a className="mb-2 md:mb-4">Home</a>
                                   </Link>
                               </li>
                               <li>
                                   <Link href="/#services">
                                        <a className="mb-2 md:mb-4">Services</a>
                                   </Link>
                               </li>
                           
                           </ul>
                          
                       </div>
                       <div className="location mr-4 pt-8">
                           <h4>Office</h4>
                           <a href="https://goo.gl/maps/jWMHy8YvmS9UhBfu6" target="_blank">2, Opeyemisi Bamidele, off Freedom Way, Eti-Osa 100276, Lagos</a>
                           <div className="flex space-x-4 pt-8 socials">
                               <a href="#">
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" clipRule="evenodd" d="M0 18.3334C0 19.2542 0.745883 20 1.66659 20H18.3334C19.2542 20 20 19.2542 20 18.3334V1.66659C20 0.745836 19.2542 0 18.3334 0H1.66663C0.745883 0 4.59233e-05 0.745882 4.59233e-05 1.66659V18.3334H0ZM10.4167 17.5V10.8334H8.75005V8.3333H10.4167C10.4167 4.03746 10.5998 3.75 15.8335 3.75V6.25002C13.075 6.25002 13.3334 6.4042 13.3334 8.33335H15.8335V10.8334H13.3334V17.5H10.4167V17.5Z" fill="#C89B41"/>
                                    </svg>
                               </a>
                               <a href="#">
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16.1352 0H3.86526C1.73394 0 0 1.73354 0 3.86485V7.94756V16.1351C0 18.2665 1.73394 20 3.86526 20H16.1356C18.2669 20 20.0004 18.2665 20.0004 16.1351V7.94715V3.86445C20 1.73313 18.2665 0 16.1352 0ZM17.2438 2.30479L17.686 2.30318V2.74332V5.69319L14.3072 5.70409L14.2955 2.31447L17.2438 2.30479ZM7.14555 7.94715C7.78619 7.06082 8.82543 6.47948 10.0002 6.47948C11.175 6.47948 12.2142 7.06082 12.8541 7.94715C13.2708 8.52567 13.5205 9.23328 13.5205 9.9998C13.5205 11.9407 11.9399 13.5197 9.99981 13.5197C8.05851 13.5197 6.47948 11.9407 6.47948 9.9998C6.47989 9.23328 6.7288 8.52567 7.14555 7.94715ZM18.0515 16.1347C18.0515 17.1921 17.1917 18.051 16.1352 18.051H3.86526C2.80827 18.051 1.94856 17.1921 1.94856 16.1347V7.94715H4.93394C4.67615 8.58134 4.53132 9.27403 4.53132 9.9998C4.53132 13.0146 6.98417 15.4691 10.0002 15.4691C13.0158 15.4691 15.4687 13.0146 15.4687 9.9998C15.4687 9.27403 15.3231 8.58134 15.0653 7.94715H18.0515V16.1347Z" fill="#C89B41"/>
                                    </svg>
                               </a>
                               <a href="#">
                                    <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M24 2.36749C23.1177 2.76965 22.1682 3.04092 21.1719 3.16231C22.1891 2.53784 22.9691 1.54868 23.3369 0.368687C22.3855 0.947694 21.3322 1.36784 20.2102 1.59464C19.3123 0.613478 18.0325 0 16.6163 0C13.8973 0 11.6928 2.26058 11.6928 5.04921C11.6928 5.44437 11.7362 5.82954 11.8205 6.19973C7.72815 5.98891 4.09963 3.97912 1.67103 0.924214C1.24718 1.66958 1.00457 2.53684 1.00457 3.46306C1.00457 5.21457 1.8737 6.76025 3.19493 7.66548C2.38816 7.63901 1.62865 7.4117 0.964618 7.03352C0.964131 7.0545 0.964131 7.07599 0.964131 7.09747C0.964131 9.54339 2.66147 11.5837 4.9142 12.0483C4.50107 12.1632 4.06601 12.2251 3.61683 12.2251C3.29919 12.2251 2.9908 12.1936 2.69021 12.1342C3.31722 14.14 5.13538 15.6002 7.28969 15.6407C5.60453 16.9951 3.48188 17.8024 1.17459 17.8024C0.777541 17.8024 0.38536 17.7784 0 17.7314C2.17965 19.1647 4.76755 20 7.5479 20C16.6051 20 21.5573 12.3065 21.5573 5.63421C21.5573 5.4154 21.5529 5.19708 21.5436 4.98027C22.5053 4.26987 23.3404 3.38013 24 2.36749Z" fill="#C89B41"/>
                                    </svg>
                               </a>
                               <a href="#">
                                    <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.259875 6.49882H4.77225V20H0.259875V6.49882ZM2.54362 0C1.01194 0 0 1.00837 0 2.33063C0 3.62412 0.97125 4.65995 2.4885 4.65995H2.51606C4.08844 4.65995 5.07281 3.62412 5.05969 2.33063C5.04525 1.00837 4.08844 0 2.54362 0ZM15.8182 6.18493C13.4177 6.18493 12.3493 7.49542 11.7574 8.42009V6.49882H7.25944C7.25944 6.49882 7.31456 7.76615 7.25944 20H11.7574V12.4653C11.7574 12.0573 11.7994 11.661 11.907 11.3628C12.2364 10.5585 12.9727 9.72796 14.2196 9.72796C15.8445 9.72796 16.5007 10.9678 16.5007 12.7792V20H21V12.2613C21 8.11928 18.7845 6.18493 15.8182 6.18493Z" fill="#C89B41"/>
                                    </svg>
                               </a>
                               <a href="#">
                                    <svg width="28" height="21" viewBox="0 0 28 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M17.8001 9.01445L11.8352 5.82254C11.4922 5.63901 11.0868 5.64586 10.7506 5.84067C10.4142 6.03568 10.2136 6.38018 10.2136 6.76254V13.0916C10.2136 13.4721 10.413 13.816 10.7471 14.0112C10.9216 14.1132 11.115 14.1643 11.3089 14.1643C11.4864 14.1643 11.6644 14.1214 11.8282 14.0352L17.7933 10.8983C18.1474 10.7119 18.3684 10.3519 18.3701 9.95848C18.3715 9.56503 18.1531 9.20342 17.8001 9.01445ZM11.7958 12.2954V7.56516L16.2539 9.95083L11.7958 12.2954Z" fill="#C89B41"/>
                                        <path d="M26.9455 4.91618L26.9443 4.90409C26.9214 4.69155 26.6938 2.80106 25.754 1.8395C24.6679 0.708513 23.4364 0.571118 22.8442 0.505241C22.7952 0.499802 22.7503 0.494765 22.7101 0.489527L22.6629 0.484692C19.0935 0.230855 13.7029 0.196204 13.6489 0.196002L13.6442 0.195801L13.6394 0.196002C13.5855 0.196204 8.19487 0.230855 4.5933 0.484692L4.54572 0.489527C4.50741 0.494564 4.46518 0.499197 4.41924 0.504435C3.83381 0.570514 2.61557 0.70811 1.52629 1.87999C0.631252 2.83128 0.372526 4.68128 0.345953 4.88918L0.342863 4.91618C0.33483 5.00462 0.144287 7.11006 0.144287 9.22376V11.1997C0.144287 13.3134 0.33483 15.4188 0.342863 15.5075L0.344305 15.5208C0.367171 15.7299 0.594586 17.5857 1.53 18.5477C2.5513 19.6408 3.84267 19.7854 4.53727 19.8632C4.64707 19.8755 4.74162 19.886 4.80609 19.897L4.86851 19.9055C6.92946 20.0973 13.3912 20.1918 13.6652 20.1956L13.6734 20.1958L13.6817 20.1956C13.7356 20.1954 19.126 20.1607 22.6955 19.9069L22.7426 19.9021C22.7878 19.8962 22.8384 19.891 22.8941 19.8854C23.4762 19.8249 24.6878 19.6994 25.7621 18.5434C26.6571 17.5919 26.916 15.742 26.9424 15.5343L26.9455 15.5073C26.9535 15.4186 27.1443 13.3134 27.1443 11.1997V9.22376C27.1441 7.11006 26.9535 5.00482 26.9455 4.91618ZM25.5619 11.1997C25.5619 13.156 25.3872 15.1708 25.3707 15.3558C25.3035 15.8652 25.0306 17.0357 24.5945 17.4993C23.9222 18.2227 23.2315 18.2944 22.7272 18.3466C22.6662 18.3529 22.6098 18.3589 22.5587 18.3651C19.1063 18.6093 13.9192 18.6468 13.6802 18.6482C13.4122 18.6444 7.04502 18.5491 5.0469 18.3676C4.94452 18.3512 4.8339 18.3388 4.71731 18.3259C4.12591 18.2596 3.31636 18.1689 2.69385 17.4993L2.67923 17.484C2.25076 17.0474 1.98565 15.9529 1.91829 15.3618C1.90573 15.222 1.72651 13.1834 1.72651 11.1997V9.22376C1.72651 7.26962 1.90078 5.25704 1.91767 5.06828C1.9978 4.46813 2.27589 3.36857 2.69385 2.92415C3.38681 2.17876 4.11746 2.09616 4.60072 2.04156C4.64686 2.03633 4.68991 2.03149 4.72967 2.02645C8.23236 1.78108 13.4567 1.74461 13.6442 1.7432C13.8316 1.74441 19.0542 1.78108 22.5257 2.02645C22.5684 2.03169 22.6149 2.03693 22.665 2.04257C23.162 2.09797 23.9133 2.18178 24.6028 2.90099L24.6091 2.90763C25.0376 3.3442 25.3027 4.45786 25.3701 5.06082C25.382 5.19278 25.5619 7.23577 25.5619 9.22376V11.1997Z" fill="#C89B41"/>
                                    </svg>
                               </a>
                           </div>
                        </div>
                        <div className="email pt-8">
                            <p>We are a team of professionals who are
excited about unique ideas and helping
your company grow. Contact us about
your needs, we are to collaborate with
you.</p>
                            <a className="mt-6 pb-b inline-block relative" href="mailto:Info@barnsandsilos.com" target="_blank" rel="noopener noreferrer">Info@barnsandsilos.com</a>

                        </div>
                   </div>
                </div> 
                <div className="copyright text-center mt-8 py-4 mx-4 md:mx-16">
                    <p>Copyright 2021 Barns &amp; Silos. All rights reserved.</p>
                </div>
            </footer>
        </>
    )
}


