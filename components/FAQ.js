// for rich text npm install @contentful/rich-text-react-renderer
import { BLOCKS } from '@contentful/rich-text-types';
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';
 
import React, {useState} from 'react'

export default function FAQ({faqs}) {
    // faq active class 
    const [activeAccordion, setActiveAccordion] = useState(0);
    //  get the position of the opened faq and assign to the active state
    const handleAccordion =(index)=>{
        setActiveAccordion(index);
    }
    // faq questions and response
    const FAQResults = faqs.map((item)=>{
      return(
        <div key={item.fields.questionNumber} className={`accordion  ${activeAccordion == item.fields.questionNumber ? "open" : " "}`}>
            {/* faq question  */}
            <button className="flex justify-between w-full items-center" accordion="accordion-1" onClick={()=>handleAccordion(item.fields.questionNumber)}>
                <p>{item.fields.enterQuestion}</p>
                <div>
                    {/* button plus and minus  */}
                    {activeAccordion == item.fields.questionNumber ? <img src="/images/minus-sign.svg" alt="minus sign"/>:<img src="/images/plus-sign.svg" alt="plus sign"/>}
                </div>
            </button>
            <div className="item_inner">
                {/* faq answer  */}
                <div className=" px-3 pb-8 text-gray-600">{documentToReactComponents(item.fields.faqAnswer)}</div>
            </div>
        </div>
      )
    })

    return (
        <>
            <div className="faq_section">
                <p className="text-center">FAQ</p>
                <h4 className="text-center py-2 mb-4">Answers to questions you have.</h4>
                <div className="accordion_container py-3 mb-8">
                    {/* faq question and answer  */}
                    {FAQResults}
                </div>
            </div>
        </>
    )
}